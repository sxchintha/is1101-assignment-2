#include <stdio.h>

int main(void) {
  float no1,no2;

  printf("Enter No 1: ");
  scanf("%f", &no1);
  printf("Enter No 2: ");
  scanf("%f", &no2);

  float mult = no1*no2;

  printf("Multiplication of the numbers = %f\n", mult);

  return 0;
}