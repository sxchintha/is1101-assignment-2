#include <stdio.h>

int main(void) {
  float r, area;

  printf("Enter the radius: ");
  scanf("%f", &r);

  area = 3.14*r*r;

  printf("Area of the disk = %f\n", area);
  return 0;
}