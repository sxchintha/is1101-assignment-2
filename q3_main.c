#include <stdio.h>

int main(void) {
  int n1,n2,tp;

  printf("Enter No 1: ");
  scanf("%d", &n1);
  printf("Enter No 2: ");
  scanf("%d", &n2);

  tp=n1;
  n1=n2;
  n2=tp;

  printf("No 1 after swap = %d\n", n1);
  printf("No 2 after swap = %d\n", n2);
  
  return 0;
}